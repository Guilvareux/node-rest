const sqlite3 = require('sqlite3').verbose();



let db = new sqlite3.Database('./db/fridge.db', (err) => {
  if(err) {
    console.error(err.message);
  }
  console.log("Connected to Fridge Database");
});

db.serialize(() => {
  //db.run('CREATE TABLE fridge(itemID INT, itemName VARCHAR(50), store VARCHAR(30), useby DATE)');
  db.run('INSERT INTO fridge(itemID, itemName, store, useby) VALUES(?,?,?,?)', ['1', 'Milk', 'LIDL', '2019-08-29']);
  db.all('SELECT * FROM fridge', [], (err, rows) => {
    if(err){
      throw err;
    }
    console.log(rows);
  });
});

db.close();
