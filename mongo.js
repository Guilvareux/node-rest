const express = require('express');
const MongoClient = require('mongodb').MongoClient;
const ObjectID = require('mongodb').ObjectID;
const url = "mongodb://localhost:27017/fridge";
const cors = require('cors');
const app = express();

app.use(express.urlencoded());
app.use(express.json());
app.use(cors());

let fridgedb;

MongoClient.connect(url, (err, db) => {
  if(err) throw err;
  fridgedb = db.db('fridge');
  startServer();
});

app.get('/listUsers', (req, res) => {
  res.statusCode = 200;
  res.header("Access-Control-Allow-Origin", "*");
  res.setHeader('Content-Type', 'text/plain');
  res.end("Yoohoo\n");
});

app.get('/listRecipies', (req, res) => {
  res.statusCode = 200;
  res.header("Access-Control-Allow-Origin", "*");
  res.setHeader('Content-Type', 'application/json');
  getRecipies().then(recipies => {
    res.end(JSON.stringify(recipies));
  });
});

app.get('/viewRecipie/:id', (req, res) => {
  res.statusCode = 200;
  res.header("Access-Control-Allow-Origin", "*");
  res.setHeader('Content-type', 'application/json');
  getRecipieIngredients(req.params.id).then(recing => {
    res.end(JSON.stringify(recing));
  });
});

app.get('/viewAllIngredients', (req, res) => {
  res.statusCode = 200;
  res.header("Access-Control-Allow-Origin", "*");
  res.setHeader('Content-type', 'application/json');
  getIngredients().then(recing => {
    res.end(JSON.stringify(recing));
  });
})

app.get('/startAgain', (req, res) => {
  res.statusCode = 200;
  nuke();
})

app.get('/firstRun', (req, res) => {
  res.statusCode = 200;
  firstRun();
})

app.post('/addRecipie', (req, res) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Origin, X-Requested-With, X-Auth-Token');
  console.log(req);
  addRecipie(req.body.name, req.body.desc, req.body.time, req.body.ingredients, req.body.steps);
});

app.post('/addIngredient', (req, res) => {
  res.header("Access-Control-Allow-Origin", "*");
  addIngredient(req.body.name, req.body.useby, req.body.lastdate);
});

app.post('/removeRecipie', (req, res) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Origin, X-Requested-With, X-Auth-Token');
  removeRecipie(req.body.id);
})


async function getRecipies() {
  let recipies = await fridgedb.collection("recipies").find({}).toArray();
  return recipies;
}

async function getRecipieIngredients(recipie_id) {
  let recipieIngredients = await fridgedb.collection("recipies").find({_id: ObjectID(recipie_id)}).toArray();
  return recipieIngredients;
}

async function getIngredients() {
  let ingredients = await fridgedb.collection("ingredients").find({}).toArray();
  return ingredients;
}

function addRecipie(name, desc, time, ingredients=[], steps=[]) {
  console.log(name + "\n" + desc + "\n" + time + "\n" + ingredients + "\n" + steps);
  let newRecipie = {
    name: name,
    desc: desc,
    time: time,
    ingredients: ingredients,
    steps: steps
  }
  fridgedb.collection("recipies").insertOne(newRecipie, (err, res) => {
    if(err) throw err;
    console.log("Recipie Added");
  });
}

function addIngredient(name, useby, lastdate) {
  let newIngredient = {
    name: name,
    useby: useby,
    lastdate: lastdate
  }
  fridgedb.collection("ingredients").insertOne(newIngredient, (err, res) => {
    if(err) throw err;
    console.log("DEBUG: Ingredient Added");
  })
}

function addRecipieIngredients(recipie_id, ingredient_id) {
  fridgedb.collection("recipies").findOne({_id: ObjectID(recipie_id)}, (err, result) => {
    if(err) throw err;
    result.ingredients.push(ingredient_id);
  });
  console.log("DEBUG: Ingredient Added to Recipie");
}

function removeRecipieIngredients(recipie_id, ingredient_id) {
  fridgedb.collection("recipies").findOne({_id: ObjectID(recipie_id)}, (err, result) => {
    if(err) throw err;
    console.log(result);
  });
  console.log("DEBUG: Ingredient Added to Recipie IGNORE");
}

function removeRecipie(recipie_id) {
  fridgedb.collection("recipies").deleteOne({_id: ObjectID(recipie_id)}, (err, result) => {
    if(err) throw err;
    if(result.deletedCount){
      console.log('ERR: No Recipies Deleted');
    };
  });
}

async function nuke() {
  await fridgedb.collection("recipies").deleteMany({}, (err, obj) => {
    if(err) throw err;
    console.log(obj.result.n + " documents(s) deleted");
    runOnce();
  })
  console.log("Nuked");
}

function runOnce() {
  let defaultSteps = [
    "This is what's known as a step"
  ]
  let defaultIngredients = [
    "Ham", "Cheese", "Tomato", "Fish", "Chips", "Curry", "Rice", "Noodles"
  ];
  addRecipie("Fish and Chips", "I love fish and chips", 15, defaultIngredients, [defaultSteps]);
  addRecipie("Pizza", "Pizza is the fuckin best", 20, defaultIngredients, [defaultSteps]);
  addRecipie("Thai Massaman Curry", "Holy mother of Thailand", 20, defaultIngredients, [defaultSteps]);
}

function firstRun() {
  fridgedb.createCollection("recipies", (err, res) => {
    if(err) throw err;
    console.log("Collection Created: Recipies!");
  });
  fridgedb.createCollection("ingredients", (err, res) => {
    if(err) throw err;
    console.log("Collection Created: Ingredients!");
  });
}

function startServer() {
  const server = app.listen(8080, () => {
    let host = server.address().address
    let port = server.address().port
    console.log(`Example app listening at http://${host}:${port}`);
  });
}