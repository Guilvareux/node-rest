const http = require('http');
const sqlite3 = require('sqlite3').verbose();
const fs = require('fs');
//const url = require('url');
const app = express();

const hostname = '127.0.0.1';
const port = 8080;

let db;


const server = http.createServer((request, response) => {
  response.writeHead(200, {'Content-Type': 'text/plain'});
  response.end('Hello World\n');
}).listen(port);

//main

setup();
//main end
db.all('SELECT * FROM fridge', [], (err, rows) => {
  if(err){
    console.log(err);
  }
  console.log(rows);
});
closeDatabase();


function setup(){
  files = fs.readdirSync('./db');
  if(files.length === 0){
    openDatabase();
    db.run('CREATE TABLE fridge(itemID INT, itemName VARCHAR(50), store VARCHAR(30), useby DATE)');
    db.run('INSERT INTO fridge(itemID, itemName, store, useby) VALUES(?,?,?,?)', ['1', 'Milk', 'LIDL', '2019-08-29']);
  } else {
    openDatabase();
  }
}


function openDatabase(){
  db = new sqlite3.Database('./db/fridge.db', (err) => {
    if(err) {
      console.error(err.message);
    }
    console.log("Connected to Fridge Database");
  });
}

function closeDatabase(){
  db.close((err) => {
    if(err){
      return console.error(err.message);
    }
    console.log('Database Closed');
  });
}