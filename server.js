const express = require('express');
const app = express();
app.use(express.urlencoded());
app.use(express.json());

app.get('/listRecipies', (req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'application/json');
  let data = getRecipies();
  res.end(JSON.stringify(data));
});

app.get('/listUsers', (req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end("Yoohoo\n");
});

app.post('/addRecipie', (req, res) => {
  console.log(req.body);
  console.log(req.body.name);
  console.log(req.body.desc);
  console.log(req.body.time);
});

app.post('/addIngredient', (req, res) => {
  console.log(req.body);
});

const server = app.listen(8080, () => {
  let host = server.address().address
  let port = server.address().port
  console.log(`Example app listening at http://${host}:${port}`);
});
