const express = require('express');
const app = express();
const fs = require('fs');
const sqlite3 = require('sqlite3').verbose();
let db;

app.use(express.urlencoded());
app.use(express.json());

app.get('/listUsers', (req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end("Yoohoo\n");
});

app.get('/listRecipies', (req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'application/json');
  let data = getRecipies();
  res.end(JSON.stringify(data));
});

app.post('/addRecipie', (req, res) => {
  console.log(req.body);
  console.log(req.body.name);
  console.log(req.body.desc);
  console.log(req.body.time);
});

app.post('/addIngredient', (req, res) => {
  console.log(req.body);
});




openDatabase();


function openDatabase() {
  db = new sqlite3.Database('./db/fridge.db'), (err) => {
    if(err) {
      console.log(err.message);
    }
  }
  db.all("SELECT name FROM sqlite_master WHERE type='table'", [], (err, rows) => {
    if(err) console.log(err.message);
    if(rows.length === 0) firstRun();
  });
}

function getRecipies() {
  let jsonRows = [];
  db.each('SELECT * FROM recipies', [], (err, row) => {
    if(err) console.log(err.message);
    let newObject = {
      name: row.itemID,
      desc: row.description,
      time: row.minutestomake,
    }
    jsonRows.push(newObject);
  });
  return jsonRows;  
}

function addRecipie(name, desc, minutes) {
  db.run('INSERT INTO recipies(itemName, description, minutestomake), VALUES(?,?,?)', [name, desc, minutes], err => {
    if(err) console.log(err.message);
  });
}


/*
function getIngredientsList(recipie_id) {
  let list = [];
  db.each(`SELECT * FROM ingredients WHERE itemID == ${recipie_id}`, [], (err, row) => {
    if(err) console.log(err.message);
    let listItem = {
      row.
    }
    list.push(row);
  });
}

function getIngredientInfo(ingredient_id) {
  db.each(`SELECT * FROM ingredients WHERE itemID == ${ingredient_id}`, [], (err, row) => {
    if(err) console.log(err.message);

  });
}*/

function firstRun() {
  db.run('CREATE TABLE recipies(itemID INT, itemName VARCHAR(50), description TEXT, minutestomake INT, PRIMARY KEY (itemID))');
  db.run('CREATE TABLE ingredients(itemID INT, itemName VARCHAR(50), foodGroup VARCHAR(50), useby DATE, bestbefore DATE, remaining INT, PRIMARY KEY (itemID))');
  db.run('CREATE TABLE ingredientslist(recipie_id INT, ingredient_id INT, quantity INT, FOREIGN KEY (recipie_id) REFERENCES recipies(itemID), FOREIGN KEY (ingredient_id) REFERENCES ingredients(itemID))');
  console.log("First Run!");
}


const server = app.listen(8080, () => {
  let host = server.address().address
  let port = server.address().port
  console.log(`Example app listening at http://${host}:${port}`);
});